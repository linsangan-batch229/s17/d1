//functions in JS are lines/blocks of codes that will the program what will be the specific tasks to do
//functions are mostly created to create complicated tasks to run seeral lines of code succession
//they are also used to prevent repeating line of codes.

function printName(){
    console.log('Ronel');
}

printName();

//sampleInvoke(); undeclared function cannot be invoke.,

//function declaration vs function expression

//this is a sample of function declaration with invoke
function declaredFunction(){
    console.log('hello world from this function');
}

//caling the function declartion
declaredFunction();

//this is a sample of function expression
//pinapasok sila sa variable
//can be stored in a variable
let b = declaredFunction;

//anonymous function - a function without a name
// variableFunction();
/*
    error - function expression being stored
    in a let or const, cannot be hoisted.
    hoisted - pwede tawagin anywhere
*/

//Anonymous function
//this is a function expression example
let variableF = function(){
    console.log('Xtra rice');
}

//this is how to call the function expression
variableF();

let functionExp = function funcName(){
    console.log('WOW');
}

//calling the function expression with function name
functionExp(); // correct - calling variable name for function  expression
//funcName();  error - function name not defined

//You can re-assign declared function and function expression
//to a new anonymous function

//declared function to anonymous function conversion
//updated the declaredFunction
declaredFunction = function(){
    console.log('mang inasal');
}

declaredFunction();


//function expression
functionExp = function(){
    console.log('jolibee');
}

//invoke or call the function
functionExp();

//However, we cannot re-assign a function expression iniitialized with const.
//sample with const initialization
const constantF = function(){
    console.log('chicken fried');
}

constantF();

// constantF = function(){
//     console.log('fries');
// }
//constantF(); - error - TypeError: Assignment to constant variable.


//function scoping
//local and global scope function
// 1. local / block scope -
// 2. global scope - hoisted anywhere
// 3. function scope - nasa loob ng function, bawal sa labas

//local variable, cannot be hoisted outside,
//only inside
{
    let localVar = "Arm";
}

//global variable, hoisted anywhere
let globalVar = "mister clean";

console.log(globalVar);
// console.log(lovalVar); -> error, not defined

//function scope
function showName(){
    // function scope var
    var functionVar = "JOE";
    const functionC = "John";
    let functionL = "Ronel";

    console.log(functionVar);
    console.log(functionC);
    console.log(functionL);
}

showName();
// console.log(functionVar); -> error - not defined


//nested function
function myNewFunc(){
    let name = "J";

    function nestedF(){
        let nestedName = "R";
        //pwede, parent to child
        console.log(name);
    }

    nestedF();
    // console.log(nestedName); -> bawal, child to parent
}

myNewFunc();
// nestedF(); - error - cannot call the nested function inside the parent function

//function and global scope variables
let globalName = "A";

function myNewFunc2(){
    let nameInside = "C";

    console.log(globalName);
}


//using alert
// aler() allow us to show small window at the top corner ofour browser

// function showAlert(){
//     alert('WOOWOW');
// }

// showAlert();

//notes on the use of alert();
    //show only alert() for short dialog message.
    //do not overuse alert() because program js has to wait for it to be dismissed before continuing

// PROMPT() allows us to show a small window and gather user input, has a input field
// usually prompt are stored in a variable
// let samplePrompt = prompt("Enter your name");

// console.log('Hellow, ' + samplePrompt);
// let x = parseInt(samplePrompt);
// console.log(x);



function printWelcomeMsg(){
    let fname = prompt("Enter your first name.");
    let lname = prompt("Enter your last name.");

    console.log("Hello, " + fname + " " + lname);
    console.log('Welcome to my page');
}



//function naming convention
//function names should be definitive of the task it will perform. It usually contains verb.

function getCourse(){
    let courses = ['Science 101', 'Math 101', 'English 101'];
    console.log(courses);
}

getCourse();